# fix-suggestions

**Merged into kharlan/fix-suggester-bot, this repo is no longer used**

A library for generating and posting fix suggestions on patches.

Used primarily by https://gitlab.wikimedia.org/KostaHarlan/fix-suggester-bot
