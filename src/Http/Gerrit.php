<?php

namespace FixSuggestions\Http;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class Gerrit {

	/** @var HttpClientInterface */
	private $client;

	/**
	 * @param string $url
	 * @param string $username
	 * @param string $password
	 */
	public function __construct( string $url, string $username, string $password ) {
		$this->client = HttpClient::createForBaseUri( $url, [
			'auth_basic' => [ $username, $password ]
		] );
	}

	/**
	 * @param array $fixSuggestions
	 * @param string $zuulProject
	 * @param int $zuulChange
	 * @param int $zuulPatchset
	 * @return ResponseInterface
	 * @throws TransportExceptionInterface
	 */
	public function postFixSuggestions(
		array $fixSuggestions,
		string $zuulProject,
		int $zuulChange,
		int $zuulPatchset
	): ResponseInterface {
		return $this->client->request( 'POST', self::getGerritUrl(
			$zuulProject,
			$zuulChange,
			$zuulPatchset,
			'review'
		), [ 'json' => [ 'robot_comments' => $fixSuggestions ] ] );
	}

	/**
	 * @param string $gerritProject
	 * @param int $gerritShortId
	 * @param int $gerritRevision
	 * @param string $endpoint
	 * @return string
	 */
	private function getGerritUrl(
		string $gerritProject, int $gerritShortId, int $gerritRevision, string $endpoint
	): string {
		return '/r/a/changes/' . urlencode( $gerritProject ) . '~' . $gerritShortId . '/revisions/'
			. $gerritRevision . '/' . $endpoint;
	}

}
