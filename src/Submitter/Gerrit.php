<?php

namespace FixSuggestions\Submitter;

use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class Gerrit {

	public const ID = 'gerrit';
	/**
	 * @var string
	 */
	private $zuulProject;
	/**
	 * @var int
	 */
	private $zuulChange;
	/**
	 * @var int
	 */
	private $zuulPatchset;
	/**
	 * @var \FixSuggestions\Http\Gerrit
	 */
	private $gerritHttpClient;

	/**
	 * @param string $gerritUrl
	 * @param string $gerritUsername
	 * @param string $gerritPassword
	 * @param string $zuulProject
	 * @param int $zuulChange
	 * @param int $zuulPatchset
	 */
	public function __construct(
		string $gerritUrl,
		string $gerritUsername,
		string $gerritPassword,
		string $zuulProject,
		int $zuulChange,
		int $zuulPatchset
	) {
		$this->gerritHttpClient = new \FixSuggestions\Http\Gerrit(
			$gerritUrl,
			$gerritUsername,
			$gerritPassword
		);

		$this->zuulProject = $zuulProject;
		$this->zuulChange = $zuulChange;
		$this->zuulPatchset = $zuulPatchset;
	}

	/**
	 * @param array $suggestions
	 * @return ResponseInterface
	 * @throws TransportExceptionInterface
	 */
	public function execute( array $suggestions ): ResponseInterface {
		return $this->gerritHttpClient->postFixSuggestions(
			$suggestions,
			$this->zuulProject,
			$this->zuulChange,
			$this->zuulPatchset
		);
	}
}
