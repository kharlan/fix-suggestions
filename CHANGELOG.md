## Next release

- ...

## v0.2.6

- [[gerrit] Use empty string instead of PHP_EOL for line removal](https://gitlab.wikimedia.org/KostaHarlan/fix-suggestions/-/merge_requests/5)

## v0.2.5

- [[gerrit] Fix path to robot comment report format](https://gitlab.wikimedia.org/KostaHarlan/fix-suggestions/-/merge_requests/4)

## v0.2.4

- Remove version from composer.json

## v0.2.3

- Tag project correctly!

## v0.2.2

- Include GerritRobotComments report format in repo

## v0.2.1

- Refactor console commands to classes that can be reused elsewhere.

## v0.2.0

- Add 'suggestions:post' command
- Rename 'generate-fix-suggestions' to 'suggestions:generate'
- [API] Add Gerrit client

## v0.1.0

- Add `generate-fix-suggestions` command.
- support for Gerrit code review suggestions format
- support for phpcs
